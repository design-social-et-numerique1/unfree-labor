# Workshop UNFREE LABOR à Saint-Luc au B9 (7-8 octobre)

Participants: 
- Design social et numérique : 6 étudiants de M1 et 6 étudiants de M2, avec Marie Sion et Olivier Evrard.
- Scénographie : 4 à 6 étudiants de M1 et 6 étudiants de M2, avec Arnaud Sohet, Florence Plihon et Noémie Drouguet
- Histoire publique (Luxembour) : 20 étudiants ( ???), avec Marie-Paule Jungblut
- Muséologie (ULiège) : ?????
- Des représentants des musées concernés : G. Assa, L. Caregari, (Kayl), A. Guarneri (Piconrue), B. Holtwick, M. Starzinger  (DASA), P. Majerus (Fonderie) (viendront-ils tous ? à confirmer).


Soit entre 40 et 50 étudiants et 12 enseignants/partenaires ; il faudra préciser le nombre dès que possible, pour des raisons d’organisation des ateliers et de « sécurité sanitaire ».
Localisation : B9 (900 m2) ; pour rassurer nos participants luxembourgeois, on y a déjà mis un éléphant, donc on aura de la place; il n’est pas inutile de dire que l’endroit sera même chauffé pour l’occasion…

# Objectifs du WS : réflexion collective et travail collaboratif 

- Je reprends les termes de Marie (document de juin) :
- Se rencontrer
- Fédérer: organiser des ateliers pour fédérer les acteurs autour de l’expo
- Co-Créer des traces des différents débats 
- Mélanger: constituer des équipes de travail pluridisciplinaires 
- Développer un processus créatif de travail et l’impliquer comme partie intégrante du projet
- Conceptualiser: imaginer une forme métaphorique du sujet en vue de son exposition 


Pour préciser ce qui est attendu concernant le concept de l’expo : partant du cadre fixé (réunion de juillet – voir ci-dessous), il faut que les étudiants 1 : précisent et développent le concept ; 2 : définissent le storytelling/la narration ; 3 : définissent un public-cible et la stratégie des publics (grandes lignes).

Pour préciser ce qui est attendu comme production artistique : une publication (a priori, de n’importe quelle nature, mais probablement comprenant une version imprimée en bonne qualité). Cette publication : garde la trace de chaque participant au WS (notamment si exercice avec Photomaton) ; garde la mémoire des échanges + de l’ambiance du WS ; reprend les résultats du WS ; sert de « carte de visite » pour le projet et sera remis aux partenaires musées ; servira de « guide » ou « feuille de route » pour les étudiants en histoire publique ; trouvera sa place au sein même l’exposition, pour témoigner du processus de conception/réalisation de l’expo (co-design). Démultiplier les supports et mettre en scène. Eviter absolument de finir les deux jours en ayant que des feuilles de brouillon ;-)

# Méthodologie de travail 

Marie et les étudiants de M2 DSN préparent les exercices pour le WS. Deux semaines pour préparer, c’est pas beaucoup mais elles vont rester concentrées ! Il faut que tout soit bien calé, avec des consignes précises et un horaire précis. Veiller à maintenir une certaine dynamique.
Les équipes seront constituée en début de jour 1, selon un protocole qui sera décidé par les étudiants de M2 DSN : 
faire intervenir le hasard, l’aléatoire, éviter que chacun reste dans sa zone de confort
réfléchir aux manières de travailler transversalement, avec les disciplines de chacun ; travail méthodologique du co-design, qui devra figurer dans le publication (sous la houlette de Florence).

# PAD collaboratif pour dialoguer entre nous

Adresse web du Pad : 
[Pad Unfree Labor](https://annuel2.framapad.org/p/workshop-unfree-labor-9j51?lang=fr)

- Vous vous rendez à l'adresse du Pad
- Vous créez un compte en haut à droite en cliquant sur l'icone avec 3 personnelles
- Vous écrivez votre nom et choisissez votre couleur
- Tout vos commentaires seront colorisés selon votre couleur

# Supports/outils de travail : 

A définir en fonction des exercices préparés par les M2 ; tout sera stocké sur un serveur GitLab ; interagir sur ce qui est en train de se faire (framapad, etc.) ; utiliser au maximum des logiciels libres, dans l’esprit open-source, optique de diffusion et d’ouverture à tout le monde (voir aussi si alternative à arcGIS ? peut-être QGIS, si une application storytelling est possible). 

# A prévoir, un « espace-ressources » 

(espace scénographié par Arnaud et étudiants de M2), pour ouvrir/alimenter des pistes pour l’expo, tant pour le sujet que pour la forme : des exemples de dispositifs numériques (Olivier), des exemples de scénographies (Arnaud, Florence), des ressources numériques ou imprimées pour nourrir le contenu (Marie-Paule et étudiants d’histoire publique, Noémie). Si les étudiants apportent des idées personnelles (sur les thématiques ou sur la forme), les travailler de façon aléatoire pourrait être à la fois créatif et décomplexant (mais il faut une méthodo très claire, une intervention très cadrée, préparée à l’avance).

# A prévoir : dispositifs pour voir la progression en live 

Voir les « choses » qui s’installent : projection (framapad, photos d’ambiance, …), impressions, collages, scénographies minutes… Tout sera capturé et placé dans le serveur.

# Intervenants

Deux intervenants seront invités, pour enrichir et rythmer les deux journées. Contacter en priorité (décroissante) (Noémie) : 
Gaëlle Henrard (Cité Miroir, potentiel partenaire) pour aborder des situations « historiques » de travail forcé et les efforts d’émancipation des travailleurs – réf. à l’exposition « En lutte », que nous pourrions d’ailleurs aller voir  ;-)
Steve Bottatin (asbl Barricade) pour aborder des situations actuelles de travail forcé ou du moins le travail qui fait souffrir, entre autres liées au « télé-travail » et à l’environnement numérique.
Ludo Bettens ou autre (IHOES – Institut d’Histoire ouvrière, économique et sociale, Seraing), pour alimenter les contenus, et peut-être proposer des documents (matériels ou audio-visuels) à exposer (storymap/ expo). 
… ?

# A faire (Noémie)

Trouver budget pour faire de bonnes impressions 
Rencontrer Stefan Askew (relations internationales) pour aide présente et future à la collaboration avec Luxembourg, Allemagne et Flandre (!). 


# Pour rappel : le cadre de l’exposition, tel qu’il a été décidé en juillet 


The goals of the exhibition
Raise awareness where labor starts to be unfree.
Unfree labor starts when concerned people do not have the choice about: payment; working hours; living/housing conditions; transport; … 
Raise awareness in which context labor turns into unfree labor.
Raise awareness that unfree labor may also exist within a legal frame.
Stimulate the audience to reflect on whom workers are dependent. 
Stimulate the audience to reflect, how we can stop unfree labor.
Topics1
Supply chains
Products / consumers
Visibility of dependence
Legal systems

 We explicitly exclude the topic of NS-concentration camps but we include the colonial heritage
